// Soal No 1
const luasPersegiPanjang = (a, b) =>{return a*b};
const kelilingPersegiPanjang = (a, b) =>{return 2*a+2*b};

console.log('Luas Persegi Panjang',luasPersegiPanjang(4,5));
console.log('Keliling Persegi Panjang',kelilingPersegiPanjang(4, 5));  

// Soal No 2
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        return firstName+' '+lastName
      }
    }
  }
   
//Driver Code 
console.log(newFunction("William", "Imoh").fullName());    

// Soal No 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject
//Driver code
console.log(firstName, lastName, address, hobby)


// Soal No 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

// Soal No 5
const planet = "earth" 
const view = "glass" 

const before =  `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet}.`
console.log(before);
