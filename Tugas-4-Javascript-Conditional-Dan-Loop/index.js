// Soal NO 1

var nilai  = 87;
var indeks;

if (nilai >=85){
    indeks = 'A';
}else if (nilai >=75 && nilai < 85) {
    indeks = 'B';
}else if (nilai >= 65 && nilai < 75) {
    indeks = 'C';
}else if (nilai >= 55 && nilai < 65) {
    indeks = 'D';
} else {
    indeks = 'E';
}

console.log(indeks);

// Soal No 2
var tanggal = 30;
var bulan = 5;
var tahun = 1999;

switch (bulan) {
    case 1:{
        console.log(tanggal,'Januari',tahun);
        break;
    }case 2:{
        console.log(tanggal,'Februari',tahun);
        break
    }case 3:{
        console.log(tanggal,'Maret',tahun);
        break
    }case 4:{
        console.log(tanggal,'April',tahun);
        break
    }case 5:{
        console.log(tanggal,'Mei',tahun);
        break
    }case 6:{
        console.log(tanggal,'Juni',tahun);
        break
    }case 7:{
        console.log(tanggal,'Juli',tahun);
        break
    }case 8:{
        console.log(tanggal,'Agustus',tahun);
        break
    }case 9:{
        console.log(tanggal,'September',tahun);
        break
    }case 10:{
        console.log(tanggal,'Oktober',tahun);
        break
    }case 11:{
        console.log(tanggal,'November',tahun);
        break
    }case 12:{
        console.log(tanggal,'Desember',tahun);
        break
    }  
    default:
        console.log('Bulan yang anda inputkan salah');
        break;
}

// Soal No 3

n = 4
for (let i = 0; i < n; i++) {
    for (let j=0; j <= i; j++){
        process.stdout.write("#");
    }
    console.log("");      
}

// Soal No 4
var m = 10
i = 1
while (i<=m) {
    if (i == 1 || i == 4 || i == 7 || i ==10) {
        console.log(i,"- I Love Programming");
    }else if(i == 2 || i == 5 || i== 8){
        console.log(i, "- I Love Javascript");
    }else{
        if (i==3) {
            console.log(i,"- I Love VueJs");
            console.log('===');
        }else if(i==6){
            console.log(i,"- I Love VueJs");
            console.log('======');
        }else if (i==9) {
            console.log(i,"- I Love VueJs");
            console.log('=========');
        }
    }
    i++
}