// Soal No 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort()
for (let index = 0; index < daftarHewan.length; index++) {
    console.log(daftarHewan[index])
}
// Batas Soal No 1

console.log('_____________________________');


// Soal No 2
function introduce(data) {
    console.log("Nama Saya",data.name,",umur saya",data.age,",alamat saya di",data.address,",dan saya punya hobby yaitu",data.hobby);
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
// Batas Soal No 4

console.log('_____________________________');

// Soal No 3
function hitung_huruf_vokal(data) {
    var res = data.toUpperCase()
    count = 0
    for (let index = 0; index < res.length; index++) {
        if (res[index] == 'I' || res[index] == 'A' || res[index] == 'U' || res[index] == 'E' || res[index] == 'O') {
            count +=1
        }
    }
    return count
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2) // 3 2
// Batas Soal No 3

console.log('_____________________________');

// Soal No 4
function hitung(angka) {
    return angka + angka - 2
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8